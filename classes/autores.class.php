<?php
class Autores{

    private $pdo;

    public function __construct($pdo){
    	$this->pdo = $pdo;
    }

    public function getTotalregistro(){

        $total =0;
        $sql = " SELECT COUNT(*) AS c FROM autores ";
        $sql = $this->pdo->query($sql);
        $sql = $sql->fetch();
        $total = $sql['c'];
        $paginas = ($total /4);
        return $paginas;
   }

   public function getListaAutores(){

        $dados = array();
        $sql = "SELECT * FROM autores ORDER BY nome ";
        $sql = $this->pdo->query($sql);
       
        if($sql->rowCount() >0){
            $dados = $sql->fetchAll();
         }
        return $dados;
   }

	public function getAutores($pg=1){

	    $dados = array();
        $pg = $pg;
        $p = ($pg -1) * 4;
		$sql = "SELECT * FROM autores ORDER BY nome LIMIT  $p,4";
        $sql = $this->pdo->query($sql);
       
        if($sql->rowCount() >0){
            $dados = $sql->fetchAll();
         }
		return $dados;
	}

	public function verificarDisponibilidade($nome){

		$sql = "SELECT * FROM autores WHERE nome =:nome";
		$sql = $this->pdo->prepare($sql);
		$sql->bindValue(":nome",$nome);
		$sql->execute();

		if($sql->rowCount() >0){
			return false;
		}else{ return true;}
	}

	public function adicionar($nome,$idUsuario){

		$sql = "INSERT INTO autores SET nome =:nome,idUsuario =:usuario ";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":nome",$nome);;
        $sql->bindValue(":usuario",$idUsuario);
        $sql->execute();
	}

	public function buscarPorId($idAutor){
		$sql = "SELECT * FROM autores WHERE idAutor =:id ";
		$sql = $this->pdo->prepare($sql);
		$sql->bindValue(":id",$idAutor);
		$sql->execute();
        $dados = array();
		if($sql->rowCount() >0){
          $dados = $sql->fetch();
		}
		return $dados;
	}

	public function atualizar($nome,$usuario,$idAutor){

		$sql = "UPDATE autores SET nome =:nome, idUsuario =:usuario  WHERE idAutor =:idAutor";
		$sql = $this->pdo->prepare($sql);
		$sql->bindValue(":nome",$nome);
		$sql->bindValue(":usuario",$usuario);
		$sql->bindValue(":idAutor",$idAutor);
		$sql->execute();
	}

	public function verificarTransacao($idAutor){

        $sql = "SELECT * FROM livros WHERE idAutor =:idAutor ";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":idAutor",$idAutor);
        $sql->execute();

        if($sql->rowCount() >0){
            return true;
        }else{return false;}
    }

    public function excluirAutor($idAutor){

        $sql = "DELETE FROM autores WHERE idAutor =:idAutor ";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":idAutor",$idAutor);
        $sql->execute();
    }




}