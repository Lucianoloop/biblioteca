<?php
class Livros{

	private $pdo;

    public function __construct($pdo){
    	$this->pdo = $pdo;
    }

    public function getLivros(){
    	$dados = array();
    	$sql = "SELECT * FROM livros";
    	$sql = $this->pdo->query($sql);

    	if($sql->rowCount() >0){
    		$dados = $sql->fetchAll();
    	}
    	return $dados;
    }

     public function getTotalregistro(){

        $total =0;
        $sql = " SELECT COUNT(*) AS c FROM livros ";
        $sql = $this->pdo->query($sql);
        $sql = $sql->fetch();
        $total = $sql['c'];
        $paginas = ceil ($total /4);
        return $paginas;
   }

    public function getLivrosPorPagina($pg=1){

        $dados = array();
        $pg = $pg;
        $p = ($pg -1) * 4;
        $sql = "SELECT l.idLivro,l.nome,l.imagem,l.ISBN ,a.nome AS nomeAutor  FROM livros AS l
                INNER JOIN autores AS a ON l.idLivro = a.idAutor
                ORDER BY nome LIMIT  $p,4";
        $sql = $this->pdo->query($sql);
       
        if($sql->rowCount() >0){
            $dados = $sql->fetchAll();
         }
        return $dados;
    }

    public function verificarDisponibilidade($nome){

        $sql = "SELECT * FROM livros WHERE nome =:nome";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":nome",$nome);
        $sql->execute();

        if($sql->rowCount() >0){
            return false;
        }else{ return true;}
    }

    public function adicionar($nome,$descricao,$isbn,$idAutor,$idUsuario,$imagem){
        
        $sql = "INSERT INTO livros SET nome =:nome, descricao =:descricao,isbn =:isbn,idAutor =:idAutor, idUsuario =:usuario, imagem =:img ";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":nome",$nome);
        $sql->bindValue(":descricao",$descricao);
        $sql->bindValue(":isbn",$isbn);
        $sql->bindValue(":idAutor",$idAutor);
        $sql->bindValue(":usuario",$idUsuario);
        $sql->bindValue(":img",$imagem);
        $sql->execute();
    }

    public function buscarPorId($idLivro){

        $sql = "SELECT * FROM livros WHERE idLivro =:id ";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":id",$idLivro);
        $sql->execute();
        $dados = array();
        if($sql->rowCount() >0){
          $dados = $sql->fetch();
        }
        return $dados;
    }

    public function atualizarLivro($nome,$descricao,$idLivro,$usuario){
        //var_dump($nome,$descricao,$idLivro,$usuario);exit;
        $sql = "UPDATE livros SET nome =:nome, descricao =:descricao, idUsuario =:usuario  WHERE idLivro =:idLivro";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":nome",$nome);
        $sql->bindValue(":descricao",$descricao);
        $sql->bindValue(":usuario",$usuario);
        $sql->bindValue(":idLivro",$idLivro);
        $sql->execute();
    }

    public function verificarTransacao($idLivro){

        $sql = "SELECT * FROM emprestimolivros WHERE idLivro =:idLivro ";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":idLivro",$idLivro);
        $sql->execute();

        if($sql->rowCount() >0){
            return true;
        }else{return false;}
    }

    public function excluirLivro($idLivro){

        $sql = "DELETE FROM livros WHERE idLivro =:idLivro ";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":idLivro",$idLivro);
        $sql->execute();
    }




    
}