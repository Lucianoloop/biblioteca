<?php
class Emprestimolivro{

    private $pdo;

    public function __construct($pdo){
    	$this->pdo = $pdo;
    }

   public function getTotalregistro(){
        $total =0;
        $sql = " SELECT COUNT(*) AS c FROM emprestimolivros ";
        $sql = $this->pdo->query($sql);
        $sql = $sql->fetch();
        $total = $sql['c'];
        $paginas = ($total /4);
        return $paginas;
   }

	public function getEmprestimolivro($pg=1){
		$dados = array();
       
        $pg = $pg;
        $p = ($pg -1) * 4;
		$sql = "SELECT em.idemprestimo, l.nome AS nlivro, p.nome AS npessoa ,em.data_incio, em.data_fim FROM emprestimolivros AS em
            INNER JOIN livros AS l ON l.idLivro = em.idLivro
            INNER JOIN pessoas AS p ON p.idPessoa = em.idPessoa  ORDER BY em.data_incio LIMIT $p,4";

        $sql = $this->pdo->query($sql);
       
        if($sql->rowCount() >0){
            $dados = $sql->fetchAll();
         }
		return $dados;
	}
    // Method verificar reserva de livro
    public function verificarDisponibilidade($livro,$data_inicio,$data_fim){

        $sql = "SELECT * FROM emprestimolivros WHERE idLivro =:livro 
                AND ( NOT(data_incio >:data_fim  OR data_fim <:data_inicio))";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":livro",$livro);
        $sql->bindValue(":data_inicio",$data_inicio);
        $sql->bindValue(":data_fim",$data_fim);
        $sql->execute();

        if($sql->rowCount() >0){
            return false;
        }else{return true;}
    }
    // Method emprestar Livro
    public function emprestar($livro,$data_inicio,$data_fim,$pessoa,$idUsuario){

        //var_dump($livro,$data_inicio,$data_fim,$pessoa,$idUsuario);exit;
        $sql = "INSERT INTO emprestimolivros SET idLivro =:idlivro, data_incio =:inicio, data_fim =:fim, idPessoa =:pessoa, idUsuario =:usuario ";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":idlivro",$livro);
        $sql->bindValue(":inicio",$data_inicio);
        $sql->bindValue(":fim",$data_fim);
        $sql->bindValue(":pessoa",$pessoa);
        $sql->bindValue(":usuario",$idUsuario);
        $sql->execute();
    }

    public function deletarEmprestimo($idemprestimo){

        $sql = "DELETE FROM emprestimolivros WHERE idemprestimo =:id";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":id",$idemprestimo);
        $sql->execute();
    }





}