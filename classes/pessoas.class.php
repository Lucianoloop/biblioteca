<?php
class Pessoas{

	private $pdo;

    public function __construct($pdo){
    	$this->pdo = $pdo;
    }

    public function getPessoas(){
    	$dados = array();
    	$sql = "SELECT * FROM pessoas";
    	$sql = $this->pdo->query($sql);

    	if($sql->rowCount() >0){
    		$dados = $sql->fetchAll();
    	}
    	return $dados;
    }

     public function getTotalregistro(){

        $total =0;
        $sql = " SELECT COUNT(*) AS c FROM pessoas ";
        $sql = $this->pdo->query($sql);
        $sql = $sql->fetch();
        $total = $sql['c'];
        $paginas = ceil ($total /4);
        return $paginas;
   }

    public function getPessoasPorPagina($pg=1){

        $dados = array();
        $pg = $pg;
        $p = ($pg -1) * 4;
        $sql = "SELECT * FROM pessoas ORDER BY nome LIMIT  $p,4";
        $sql = $this->pdo->query($sql);
       
        if($sql->rowCount() >0){
            $dados = $sql->fetchAll();
         }
        return $dados;
    }

    public function verificarDisponibilidade($nome){

        $sql = "SELECT * FROM pessoas WHERE nome =:nome";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":nome",$nome);
        $sql->execute();

        if($sql->rowCount() >0){
            return false;
        }else{ return true;}
    }

    public function adicionar($nome,$email,$telefone,$idUsuario){

        $sql = "INSERT INTO pessoas SET nome =:nome,email =:email,telefone =:telefone,idUsuario =:usuario ";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":nome",$nome);
        $sql->bindValue(":email",$email);
        $sql->bindValue(":telefone",$telefone);
        $sql->bindValue(":usuario",$idUsuario);
        $sql->execute();
    }

    public function buscarPorId($idPessoa){
        $sql = "SELECT * FROM pessoas WHERE idPessoa =:id ";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":id",$idPessoa);
        $sql->execute();
        $dados = array();
        if($sql->rowCount() >0){
          $dados = $sql->fetch();
        }
        return $dados;
    }

    public function atualizar($nome,$telefone,$usuario,$idPessoa){

        $sql = "UPDATE pessoas SET nome =:nome,telefone =:tel, idUsuario =:usuario  WHERE idPessoa =:idPessoa";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":nome",$nome);
        $sql->bindValue(":tel",$telefone);
        $sql->bindValue(":usuario",$usuario);
        $sql->bindValue(":idPessoa",$idPessoa);
        $sql->execute();
    }

    public function verificarTransacao($idPessoa){

        $sql = "SELECT * FROM emprestimolivros WHERE idPessoa =:idPessoa ";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":idPessoa",$idPessoa);
        $sql->execute();

        if($sql->rowCount() >0){
            return true;
        }else{return false;}
    }

    public function excluirPessoa($idPessoa){

        $sql = "DELETE FROM pessoas WHERE idPessoa =:idPessoa ";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":idPessoa",$idPessoa);
        $sql->execute();
    }



    
}