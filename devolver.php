<?php
session_start();
require_once 'config.php';
require_once 'classes/emprestimolivro.class.php';
$emprestimo = new Emprestimolivro($pdo);

if(!empty($_GET['id'])){
  $idemprestimo = addslashes($_GET['id']);
  $emprestimo->deletarEmprestimo($idemprestimo);
  header("Location: index.php");
}
?>
