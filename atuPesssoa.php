<?php
session_start();
require_once 'config.php';
require_once 'classes/pessoas.class.php';
require_once 'head.php';

$pessoas = new Pessoas($pdo);
if(!empty($_GET['id'])){
    $idPessoa = addslashes($_GET['id']);
    $dados = $pessoas->buscarPorId($idPessoa);  

if(!empty($_POST['nome'])){
	$nome =  addslashes($_POST['nome']);
	$telefone =  addslashes($_POST['telefone']);
	$pessoas->atualizar($nome,$telefone,$_SESSION['lg'],$idPessoa);
	header("Location: pessoas.php");exit;	
}     
?>
<div class="container">
	<form method="POST">
		<h2>Editar <small>Pessoa</small> </h2>
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
                  <label for="data_inicio">Nome</label>
                  <input type="text" class="form-control" name="nome" value="<?php echo $dados['nome'];?>" required>
                </div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
                  <label for="data_inicio">Telefone</label>
                  <input type="text" class="form-control" name="telefone" value="<?php echo $dados['telefone'];?>" required>
                </div>
			</div>
			<div class="col-sm-12">
				<div class="form-group">
                    <button type="submit" class="btn btn-primary mb-2">Atualizar</button>
                </div>
			</div>
			
		</div>
    </form>
</div>
<?php } require_once 'footer.php';?>