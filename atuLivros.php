<?php
session_start();
require_once 'config.php';
require_once 'classes/livros.class.php';
require_once 'head.php';

$livros = new Livros($pdo);
if(!empty($_GET['id'])){
    $idLivro = addslashes($_GET['id']);
    $dados = $livros->buscarPorId($idLivro);  

 
if(!empty($_POST['nome'])){
	$nome =  addslashes($_POST['nome']);
	$descricao = addslashes($_POST['descricao']);
	$livros->atualizarLivro($nome,$descricao,$idLivro,$_SESSION['lg']);
	header("Location: livros.php");exit;	
}     
?>
<div class="container">
	<form method="POST">
		<h2>Editar <small>Livro</small> </h2>
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
                  <label for="data_inicio">Nome</label>
                  <input type="text" class="form-control" name="nome" value="<?php echo $dados['nome'];?>" required>
                </div>
			</div>
			<div class="col-sm-12">
			<div class="form-group">
               <label for="comment">Descrição</label>
               <textarea class="form-control" rows="2" name="descricao" required><?php echo $dados['descricao'];?>
               </textarea>
            </div>
            </div>
			<div class="col-sm-12">
				<div class="form-group">
                    <button type="submit" class="btn btn-primary mb-2">Atualizar</button>
                </div>
			</div>
			
		</div>
    </form>
</div>
<?php } require_once 'footer.php';?>