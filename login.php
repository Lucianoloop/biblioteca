
<?php
session_start();
require_once 'config.php';
 $_SESSION['lg'] = '';

if(isset($_POST['email']) && !empty($_POST['email'])){
  $email = addslashes($_POST['email']);
  $senha = md5(addslashes($_POST['senha']));

  $sql = "SELECT * FROM usuarios WHERE email =:email AND senha =:senha ";
  $sql = $pdo->prepare($sql);
  $sql->bindValue(":email",$email);
  $sql->bindValue(":senha",$senha);
  $sql->execute();

  if($sql->rowCount()>0){
    $sql = $sql->fetch();
    $id = $sql['idUsuario'];
    $ip = $_SERVER['REMOTE_ADDR'];
    $_SESSION['lg'] = $id;
    $sql = "UPDATE usuarios SET ip =:ip WHERE idUsuario =:id";
    $sql = $pdo->prepare($sql);
    $sql->bindValue(":ip",$ip);
    $sql->bindValue(":id",$id);
    $sql->execute();
    header("Location: index.php");exit;

  }

}
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="pagina teste">
    <meta name="author" content="Luciano Dias">
    <link rel="icon" href="assets/icones/favicon.png">
    <title>Login</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <link href="assets/css/signin.css" rel="stylesheet">
    <script src="assets/js/ie-emulation-modes-warning.js"></script>
  </head>
  <body>
    <div class="container">

      <form class="form-signin" method="POST">
        <h2 class="form-signin-heading">Login</h2>
        <label for="email" >E-mail</label>
        <input type="email" name="email" class="form-control" placeholder="Informe seu email" required autofocus>
        <label for="senha">Senha</label>
        <input type="password" name="senha" class="form-control" placeholder="Informe sua senha" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
      </form>
    </div> <!-- /container -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
