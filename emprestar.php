<?php
session_start();
require_once 'config.php';
require_once 'classes/livros.class.php';
require_once 'classes/emprestimolivro.class.php';
require_once 'classes/pessoas.class.php';
require_once 'head.php';

$emprestimo = new Emprestimolivro($pdo);
$livros = new Livros($pdo);
$pessoas = new Pessoas($pdo);
$listaLivro = $livros->getLivros();
$listaPessoa = $pessoas->getPessoas();   

if(!empty($_POST['livro'])){
	$livro =  addslashes($_POST['livro']);
	$data_inicio =  addslashes($_POST['data_incio']);
	$data_fim =  addslashes($_POST['data_fim']);
	$pessoa =  addslashes($_POST['pessoa']);

	if($emprestimo->verificarDisponibilidade($livro,$data_inicio,$data_fim)){
		$emprestimo->emprestar($livro,$data_inicio,$data_fim,$pessoa,$_SESSION['lg']);
		header("Location: index.php");exit;
	}else{
		echo "<div class='container'>
		<div class='alert alert-danger'>Este </strong>Livro</strong> já está emprestado neste período.</div>
		</div>";
	}
}           
?>
<div class="container">
	<form method="POST">
		<h2>Novo <small>Emprestímo</small> </h2>
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
                  <label for="livro">Livro</label>
                  <select class="form-control" name="livro" autofocus>
                     <?php foreach($listaLivro as $livro):?> 
                     	<option value="<?php echo $livro['idLivro'];?>">
                     		<?php echo $livro['nome'];?></option>
                     <?php endforeach;?>
                 </select>
              </div>
			</div>
			<div class="col-sm-3">
				<div class="form-group">
                  <label for="data_inicio">Data de início</label>
                  <input type="date" class="form-control" name="data_incio" required>
                </div>
			</div>
			<div class="col-sm-3">
				<div class="form-group">
                  <label for="data_fim">Data de final</label>
                  <input type="date" class="form-control" name="data_fim" required>
                </div>
			</div>
			<div class="col-sm-8">
				<div class="form-group">
                  <label for="pessoa">Pessoa</label>
                  <select class="form-control" name="pessoa" >
                     <?php foreach($listaPessoa as $pessoa):?> 
                     	<option value="<?php echo $pessoa['idPessoa'];?>">
                     		<?php echo utf8_encode($pessoa['nome']);?></option>
                     <?php endforeach;?>
                 </select>
              </div>
              <button type="submit" class="btn btn-primary mb-2">Confirmar emprestimo</button>
			</div>

		</div>
    </form>
</div>
<?php require_once 'footer.php';?>