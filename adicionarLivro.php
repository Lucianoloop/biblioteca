<?php
session_start();
require_once 'config.php';
require_once 'classes/livros.class.php';
require_once 'classes/autores.class.php';
require_once 'head.php';

$livros = new Livros($pdo);
$autores = new Autores($pdo);
$listaAutor = $autores->getListaAutores(); 

if(!empty($_POST['nome'])){
	$nome =  addslashes($_POST['nome']);
	$descricao =  addslashes($_POST['descricao']);
	$isbn =  addslashes($_POST['isbn']);
	$idAutor =  addslashes($_POST['autor']);
	$imagem = "default.jpeg";
	$arquivo = $_FILES['arquivo'];
	if(isset($arquivo['tmp_name']) && !empty($arquivo['tmp_name'])){
		$nomeimg = Date("d-m-Y-h-i-s")."_";
        $nomeimg .=$arquivo['name'];
		move_uploaded_file($arquivo['tmp_name'],'assets/img/'.$nomeimg);
		$imagem = $nomeimg;
	}
	if($livros->verificarDisponibilidade($nome)){
		$livros->adicionar($nome,$descricao,$isbn,$idAutor,$_SESSION['lg'],$imagem);
		header("Location: livros.php");exit;
	}else{
		echo "<div class='container'>
		<div class='alert alert-danger'>Este </strong>Livro</strong> já está cadastrado.</div>
		</div>";
	}
}           
?>
<div class="container">
	<form method="POST" enctype="multipart/form-data">
		<h2>Nova <small>Livro</small> </h2>
		<div class="row">
			<div class="col-sm-5">
				<div class="form-group">
                  <label for="data_inicio">Nome</label>
                  <input type="text" class="form-control" name="nome"  required>
                </div>
			</div>
			<div class="col-sm-5">
				<div class="form-group">
				   <label for="arquivo">Imagem do Livro</label>
                   <input type="file" class="form-control-file" name="arquivo">
                </div>
			</div>
			<div class="col-sm-12">
			<div class="form-group">
               <label for="comment">Descrição</label>
               <textarea class="form-control" rows="2" name="descricao" required></textarea>
            </div>
            </div>
			<div class="col-sm-2">
				<div class="form-group">
                  <label for="data_inicio">ISBN</label>
                  <input type="text" class="form-control" name="isbn" maxlength="8"  required>
                </div>
			</div>
			<div class="col-sm-5">
				<div class="form-group">
                  <label for="autor">Autor</label>
                  <select class="form-control" name="autor" >
                     <?php foreach($listaAutor as $autor):?> 
                     	<option value="<?php echo $autor['idAutor'];?>">
                     		<?php echo utf8_encode($autor['nome']);?></option>
                     <?php endforeach;?>
                 </select>
              </div>
          </div>
			<div class="col-sm-12">
				<div class="form-group">
                    <button type="submit" class="btn btn-primary mb-2">Cadastrar</button>
                </div>
			</div>
			
		</div>
    </form>
</div>
<?php require_once 'footer.php';?>