<?php
session_start();
require_once 'config.php';
require_once 'head.php';
require_once 'classes/livros.class.php';
$livros = new Livros($pdo);

if(!empty($_GET['id']) ){
	$id =  addslashes($_GET['id']);
	if($livros->verificarTransacao($id)){
       $_SESSION['msg'] = "<div class='alert alert-danger'>Este Livro não pode ser apagado, pois existem transações </div>";
	}else{
		$livros->excluirLivro($id);
		 $_SESSION['msg'] = "<div class='alert alert-success'>Este Livro foi excluida com sucesso!  </div>";
	}
    header("Location: livros.php");
}