<?php
session_start();
require_once 'config.php';
require_once 'classes/autores.class.php';
require_once 'head.php';

$autores = new Autores($pdo);
if(!empty($_GET['id'])){
    $idAutor = addslashes($_GET['id']);
    $dados = $autores->buscarPorId($idAutor);  

if(!empty($_POST['nome'])){
	$nome =  addslashes($_POST['nome']);
	$autores->atualizar($nome,$_SESSION['lg'],$idAutor);
	header("Location: autores.php");exit;	
}     
?>
<div class="container">
	<form method="POST">
		<h2>Editar <small>Autor</small> </h2>
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
                  <label for="data_inicio">Nome</label>
                  <input type="text" class="form-control" name="nome" value="<?php echo $dados['nome'];?>" required>
                </div>
			</div>
			<div class="col-sm-12">
				<div class="form-group">
                    <button type="submit" class="btn btn-primary mb-2">Atualizar</button>
                </div>
			</div>
			
		</div>
    </form>
</div>
<?php } require_once 'footer.php';?>