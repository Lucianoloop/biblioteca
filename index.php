<?php
session_start();
require_once 'config.php';
require_once 'classes/emprestimolivro.class.php';

if( empty($_SESSION['lg'])){
  header("Location: login.php");exit;
}else{
	$id = $_SESSION['lg'];
	$ip = $_SERVER['REMOTE_ADDR'];
	$sql = "SELECT * FROM usuarios WHERE idUsuario =:id AND ip = :ip ";
	$sql = $pdo->prepare($sql);
	$sql->bindValue(":id",$id);
	$sql->bindValue(":ip",$ip);
	$sql->execute();

	if($sql->rowCount() == 0){
		header("Location: login.php");exit;
	}
}
require_once 'head.php';

$emprestimo = new Emprestimolivro($pdo);

if(isset($_GET['p']) && !empty($_GET['p'])){
	$p = addslashes($_GET['p']);
    $lista = $emprestimo->getEmprestimolivro($p);
}else{$lista = $emprestimo->getEmprestimolivro();}
?>
<div class="container">
	<a href="emprestar.php"> <button class="btn btn-primary">Emprestar livro</button></a>
	<br><br>
	<table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">Codigo</th>
      <th scope="col">Livro</th>
      <th scope="col">Pessoa</th>
      <th scope="col">Data incio</th>
      <th scope="col">Data final</th>
      <th scope="col">Ações</th>
    </tr>
  </thead>
  <tbody>
 <?php  foreach($lista as $emprest):?>
    <tr>
      <th scope="row"><?php echo $emprest['idemprestimo'];?></th>
      <td><?php echo $emprest['nlivro'];?></td>
      <td><?php echo $emprest['npessoa'];?></td>
      <td><?php echo Date("d/m/Y", strtotime($emprest['data_incio']));?></td>
      <td><?php echo Date("d/m/Y", strtotime($emprest['data_fim']));?> </td>
      <td><a href=" devolver.php?id=<?php echo $emprest['idemprestimo'];?>" class="btn btn-default btn-sm">Devolver</a></td>
    </tr>
<?php endforeach;?> 
  </tbody>
</table>
<?php $paginas = $emprestimo->getTotalregistro();
for($q=0;$q<$paginas;$q++){
    echo '<ul class="pagination">';
	echo '<li><a href="./?p='.($q+1).'">'.($q+1).' </a></li>';
	echo '</ul>';
} 


?>
</div>








<?php require_once 'footer.php';?>