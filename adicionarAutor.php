<?php
session_start();
require_once 'config.php';
require_once 'classes/autores.class.php';
require_once 'head.php';

$autores = new Autores($pdo);

if(!empty($_POST['nome'])){
	$nome =  addslashes($_POST['nome']);

	if($autores->verificarDisponibilidade($nome)){
		$autores->adicionar($nome,$_SESSION['lg']);
		header("Location: autores.php");exit;
	}else{
		echo "<div class='container'>
		<div class='alert alert-danger'>Este </strong>Autor</strong> já está cadastrado.</div>
		</div>";
	}
}           
?>
<div class="container">
	<form method="POST">
		<h2>Novo <small>Autor</small> </h2>
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
                  <label for="data_inicio">Nome</label>
                  <input type="text" class="form-control" name="nome"  required>
                </div>
			</div>
			<div class="col-sm-12">
				<div class="form-group">
                    <button type="submit" class="btn btn-primary mb-2">Cadastrar</button>
                </div>
			</div>
			
		</div>
    </form>
</div>
<?php require_once 'footer.php';?>