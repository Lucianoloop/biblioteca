-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 08-Abr-2019 às 15:39
-- Versão do servidor: 5.7.24
-- versão do PHP: 7.1.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `biblioteca`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `autores`
--

DROP TABLE IF EXISTS `autores`;
CREATE TABLE IF NOT EXISTS `autores` (
  `idAutor` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  PRIMARY KEY (`idAutor`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `autores`
--

INSERT INTO `autores` (`idAutor`, `nome`, `idUsuario`) VALUES
(1, 'Almeida Garrett', 1),
(2, 'Drauzio Varella', 1),
(3, 'John Green', 1),
(4, 'Marissa Meyer', 1),
(5, 'Monteiro Lobato', 1),
(6, 'Stephen King', 1),
(7, 'Sophia de Mello', 1),
(8, 'Valter Hugo ', 1),
(17, 'Luciano Dias', 1),
(18, 'Maria Clara', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `emprestimolivros`
--

DROP TABLE IF EXISTS `emprestimolivros`;
CREATE TABLE IF NOT EXISTS `emprestimolivros` (
  `idemprestimo` int(11) NOT NULL AUTO_INCREMENT,
  `idLivro` int(11) NOT NULL,
  `data_incio` date NOT NULL,
  `data_fim` date NOT NULL,
  `idPessoa` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  PRIMARY KEY (`idemprestimo`),
  KEY `FK_Livro` (`idLivro`),
  KEY `FK_Pessoa` (`idPessoa`),
  KEY `FK_Usuario` (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `emprestimolivros`
--

INSERT INTO `emprestimolivros` (`idemprestimo`, `idLivro`, `data_incio`, `data_fim`, `idPessoa`, `idUsuario`) VALUES
(2, 2, '2019-04-06', '2019-04-06', 1, 1),
(3, 2, '2019-04-08', '2019-04-08', 2, 1),
(4, 2, '2019-04-09', '2019-04-09', 3, 1),
(5, 1, '2019-04-09', '2019-04-09', 2, 1),
(6, 2, '2019-04-10', '2019-04-10', 1, 1),
(7, 1, '2019-04-11', '2019-04-11', 3, 1),
(10, 5, '2019-04-08', '2019-04-08', 7, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `livros`
--

DROP TABLE IF EXISTS `livros`;
CREATE TABLE IF NOT EXISTS `livros` (
  `idLivro` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `descricao` varchar(100) DEFAULT NULL,
  `imagem` varchar(30) DEFAULT NULL,
  `isbn` varchar(15) DEFAULT NULL,
  `idAutor` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  PRIMARY KEY (`idLivro`),
  KEY `idAutor` (`idAutor`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `livros`
--

INSERT INTO `livros` (`idLivro`, `nome`, `descricao`, `imagem`, `isbn`, `idAutor`, `idUsuario`) VALUES
(1, 'A RevoluÃ§Ã£o Dos Bichos', 'Livro muito Bom Indicado para ler agora                                              ', 'default.jpeg', '12155', 2, 1),
(2, 'Livro X', 'dsfsfsfsfsfdsf', 'martin.jpg', 'dsd', 2, 1),
(4, 'Luciano Dias', 'DescriÃ§Ã£o para teste', '08-04-2019-01-50-49_vento.jpg', '1212515', 4, 1),
(5, 'A cabana', 'dasdasdsad', '08-04-2019-01-56-02_vento.jpg', 'sss', 1, 1),
(7, 'HARRY POTTER', 'HARRY POTTERHARRY POTTERHARRY POTTER', '08-04-2019-01-58-48_HARRY.jpg', '21212112', 6, 1),
(8, 'Hardware total', 'Sobre computadores em geral', 'default.jpeg', '122333', 8, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoas`
--

DROP TABLE IF EXISTS `pessoas`;
CREATE TABLE IF NOT EXISTS `pessoas` (
  `idPessoa` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `telefone` varchar(15) DEFAULT NULL,
  `idUsuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`idPessoa`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pessoas`
--

INSERT INTO `pessoas` (`idPessoa`, `nome`, `email`, `telefone`, `idUsuario`) VALUES
(1, 'Fulano da Silva', 'fulano@gmail.com', '(31)3394-7852', NULL),
(2, 'Cicrano de Paula', 'cicrano@gmail', '(31)3394-7895', NULL),
(3, 'Pedro Lucas', 'pedro@gmail.com', '(31)33557-9851', NULL),
(6, 'C#', 'java@gmail.com', '(31)3394-7895', 1),
(7, 'Luciano Dias', 'java@gmail.com', '(31)3394-7895', 1),
(9, 'Marketing', 'java@gmail.com', '(31)3394-7895', 1),
(10, 'php', 'admin@gmail.com', '(31)3394-7895', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` varchar(32) NOT NULL,
  `ip` varchar(20) NOT NULL,
  PRIMARY KEY (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`idUsuario`, `nome`, `email`, `senha`, `ip`) VALUES
(1, 'admin', 'admin@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '::1');

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `emprestimolivros`
--
ALTER TABLE `emprestimolivros`
  ADD CONSTRAINT `FK_Livro` FOREIGN KEY (`idLivro`) REFERENCES `livros` (`idLivro`),
  ADD CONSTRAINT `FK_Pessoa` FOREIGN KEY (`idPessoa`) REFERENCES `pessoas` (`idPessoa`),
  ADD CONSTRAINT `FK_Usuario` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`idUsuario`);

--
-- Limitadores para a tabela `livros`
--
ALTER TABLE `livros`
  ADD CONSTRAINT `FK_Autor` FOREIGN KEY (`idAutor`) REFERENCES `autores` (`idAutor`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
