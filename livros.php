<?php
session_start();
require_once 'config.php';
require_once 'head.php';
require_once 'classes/pessoas.class.php';
require_once 'classes/livros.class.php';
$livros = new Livros($pdo);

if(isset($_GET['p']) && !empty($_GET['p'])){
  $p = addslashes($_GET['p']);
    $lista = $livros->getLivrosPorPagina($p);
}else{$lista = $livros->getLivrosPorPagina();}
?>
<div class="container">
  <a href="adicionarLivro.php"> <button class="btn btn-primary">Adicionar Livro</button></a>
  <br><br>
  <?php
     if(isset($_SESSION['msg'])){
         echo"<div class='container'>".$_SESSION['msg'];
         unset($_SESSION['msg']);
     }
  ?>
    <table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">Codigo</th>
      <th scope="col">Imagem</th>
      <th scope="col">Nome</th>
      <th scope="col">ISBN</th>
      <th scope="col">Autor</th>
      <th scope="col">Ações</th>
    </tr>
  </thead>
  <tbody>
 <?php  foreach($lista as $livro):?>
    <tr>
      <th scope="row"><?php echo  $livro['idLivro'];?></th>
      <td><img src="assets/img/<?php echo $livro['imagem'];?>" class="img-thumbnail" width="40"></td>
      <td><?php echo $livro['nome'];?></td>
      <td><?php echo $livro['ISBN'];?></td>
      <td><?php echo $livro['nomeAutor'];?></td>
      <td><a href="atuLivros.php?id=<?php echo $livro['idLivro'];?>" class="btn btn-default btn-sm">Atualizar</a>
      <a href="delLivros.php?id=<?php echo $livro['idLivro'];?>" class="btn btn-danger btn-sm">Deletar</a></td>
    </tr>
<?php endforeach;?> 
  </tbody>
</table>
<?php  $paginas = $livros->getTotalregistro();
for($q=0;$q<$paginas;$q++){
    echo '<ul class="pagination">';
    echo '<li><a href="./livros.php?p='.($q+1).'">'.($q+1).' </a></li>';
    echo '</ul>';
} 
?>
</div>
<?php require_once 'footer.php';?>


