<?php
session_start();
require_once 'config.php';
require_once 'head.php';
require_once 'classes/autores.class.php';

$autores = new Autores($pdo);

if(isset($_GET['p']) && !empty($_GET['p'])){
	$p = addslashes($_GET['p']);
    $lista = $autores->getAutores($p);
}else{$lista = $autores->getAutores();}
?>
<div class="container">
	<a href="adicionarAutor.php"> <button class="btn btn-primary">Adicionar Autor</button></a>
	<br><br>
  <?php
     if(isset($_SESSION['msg'])){
         echo"<div class='container'>".$_SESSION['msg'];
         unset($_SESSION['msg']);
     }
  ?>
    <table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">Codigo</th>
      <th scope="col">Nome</th>
      <th scope="col">Ações</th>
    </tr>
  </thead>
  <tbody>
 <?php  foreach($lista as $autor):?>
    <tr>
      <th scope="row"><?php echo  $autor['idAutor'];?></th>
      <td><?php echo utf8_encode( $autor['nome']);?></td>
      <td><a href=" atuAutor.php?id=<?php echo  $autor['idAutor'];?>" class="btn btn-default btn-sm">Atualizar</a>
      <a href=" delAutor.php?id=<?php echo  $autor['idAutor'];?>" class="btn btn-danger btn-sm">Deletar</a></td>
    </tr>
<?php endforeach;?> 
  </tbody>
</table>
<?php  $paginas = $autores->getTotalregistro();
for($q=0;$q<$paginas;$q++){
    echo '<ul class="pagination">';
	echo '<li><a href="./autores.php?p='.($q+1).'">'.($q+1).' </a></li>';
	echo '</ul>';
} 
?>
</div>
<?php require_once 'footer.php';?>


