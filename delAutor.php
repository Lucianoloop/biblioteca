<?php
session_start();
require_once 'config.php';
require_once 'classes/autores.class.php';

$autores = new Autores($pdo);

if(!empty($_GET['id']) ){
	$id =  addslashes($_GET['id']);
	if($autores->verificarTransacao($id)){
       $_SESSION['msg'] = "<div class='alert alert-danger'>Este autor não pode ser apagado, pois existem transações </div>";
	}else{
		$autores->excluirAutor($id);
		 $_SESSION['msg'] = "<div class='alert alert-success'>Este autor foi excluida com sucesso!  </div>";
	}
    header("Location: autores.php");
}
?>