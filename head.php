<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="pagina teste">
    <meta name="author" content="Luciano Dias">
    <link rel="icon" href="assets/icones/favicon.png">
    <title>Biblioteca</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
  </head>
<body>
  <nav class="navbar navbar-inverse">
     <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="index.php">Biblioteca teste</a>
        </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="index.php">Livros emprestado</a></li>
      <li><a href="autores.php">Autor</a></li>
      <li><a href="livros.php">Livros</a></li>
      <li><a href="pessoas.php">Pessoas</a></li>
      
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="./sair.php"><span class="glyphicon glyphicon-user"></span> Sair</a></li>
    </ul>
  </div>
</nav>