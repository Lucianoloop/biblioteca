<?php
session_start();
require_once 'config.php';
require_once 'head.php';
require_once 'classes/pessoas.class.php';
$pessoas = new Pessoas($pdo);

if(!empty($_GET['id']) ){
	$id =  addslashes($_GET['id']);
	if($pessoas->verificarTransacao($id)){
       $_SESSION['msg'] = "<div class='alert alert-danger'>Esta pessoa não pode ser apagado, pois existem transações </div>";
	}else{
		 $pessoas->excluirPessoa($id);
		 $_SESSION['msg'] = "<div class='alert alert-success'>Esta pessoa foi excluida com sucesso!  </div>";
	}

    header("Location: pessoas.php");
}