<?php
session_start();
require_once 'config.php';
require_once 'classes/pessoas.class.php';
require_once 'head.php';

$pessoas = new Pessoas($pdo);

if(!empty($_POST['nome'])){
	$nome =  addslashes($_POST['nome']);
	$email =  addslashes($_POST['email']);
	$telefone =  addslashes($_POST['telefone']);

	if($pessoas->verificarDisponibilidade($nome)){
		$pessoas->adicionar($nome,$email,$telefone,$_SESSION['lg']);
		header("Location: pessoas.php");exit;
	}else{
		echo "<div class='container'>
		<div class='alert alert-danger'>Esta </strong>Pessoa</strong> já está cadastrado.</div>
		</div>";
	}
}           
?>
<div class="container">
	<form method="POST">
		<h2>Nova <small>Pessoa</small> </h2>
		<div class="row">
			<div class="col-sm-5">
				<div class="form-group">
                  <label for="data_inicio">Nome</label>
                  <input type="text" class="form-control" name="nome"  required>
                </div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
                  <label for="data_inicio">E-mail</label>
                  <input type="email" class="form-control" name="email"  required>
                </div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
                  <label for="data_inicio">Telefone</label>
                  <input type="tel" class="form-control" name="telefone"  required>
                </div>
			</div>

			<div class="col-sm-12">
				<div class="form-group">
                    <button type="submit" class="btn btn-primary mb-2">Cadastrar</button>
                </div>
			</div>
			
		</div>
    </form>
</div>
<?php require_once 'footer.php';?>