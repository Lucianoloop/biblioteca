<?php
session_start();
require_once 'config.php';
require_once 'head.php';
require_once 'classes/pessoas.class.php';

$pessoas = new Pessoas($pdo);

if(isset($_GET['p']) && !empty($_GET['p'])){
	$p = addslashes($_GET['p']);
    $lista = $pessoas->getPessoasPorPagina($p);
}else{$lista = $pessoas->getPessoasPorPagina();}
?>
<div class="container">
	<a href="adicionarPessoa.php"> <button class="btn btn-primary">Adicionar Pessoa</button></a>
	<br><br>
  <?php
     if(isset($_SESSION['msg'])){
         echo"<div class='container'>".$_SESSION['msg'];
         unset($_SESSION['msg']);
     }
  ?>
    <table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">Codigo</th>
      <th scope="col">Nome</th>
      <th scope="col">E-mail</th>
      <th scope="col">Telefone</th>
      <th scope="col">Ações</th>
    </tr>
  </thead>
  <tbody>
 <?php  foreach($lista as $pessoa):?>
    <tr>
      <th scope="row"><?php echo  $pessoa['idPessoa'];?></th>
      <td><?php echo utf8_encode( $pessoa['nome']);?></td>
      <td><?php echo utf8_encode( $pessoa['email']);?></td>
      <td><?php echo utf8_encode( $pessoa['telefone']);?></td>
      <td><a href=" atuPesssoa.php?id=<?php echo $pessoa['idPessoa'];?>" class="btn btn-default btn-sm">Atualizar</a>
      <a href=" delPessoa.php?id=<?php echo  $pessoa['idPessoa'];?>" class="btn btn-danger btn-sm">Deletar</a></td>
    </tr>
<?php endforeach;?> 
  </tbody>
</table>
<?php  $paginas = $pessoas->getTotalregistro();
for($q=0;$q<$paginas;$q++){
    echo '<ul class="pagination">';
	echo '<li><a href="./pessoas.php?p='.($q+1).'">'.($q+1).' </a></li>';
	echo '</ul>';
} 
?>
</div>
<?php require_once 'footer.php';?>


